import { fetchBaseQuery } from "@reduxjs/toolkit/dist/query";

export const baseQueryConfig = fetchBaseQuery({
    baseUrl: process.env.REACT_APP_BASE_URL_API,
    prepareHeaders: (headers, { getState }) => {
        headers.set('Content-Type', 'application/json;');
        headers.set('Accept', 'application/json');
        headers.set('Access-Control-Allow-Origin', '*')
        headers.set('Access-Control-Allow-Headers', "*")
        return headers
    },
})