import { sportsApi } from './api/sports/sports.api';
import { configureStore } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import { resultsApi } from './api/results/results.api'
import reducer from './reducers'

export const store = configureStore({
  reducer,
  devTools: process.env.NODE_ENV === 'development',
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({}).concat([
    resultsApi.middleware,
    sportsApi.middleware,
  ]),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>();
