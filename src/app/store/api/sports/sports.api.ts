import { ISports } from './sports.type';
import { createApi } from '@reduxjs/toolkit/query/react'
import { baseQueryConfig } from '../../config/baseQuery'


type getSportsType = {
    selectbox?: string
}

export const sportsApi = createApi({
    reducerPath: "sports",
    tagTypes: ['sports'],
    baseQuery: baseQueryConfig,
    endpoints: (build) => ({

        getSports: build.query<ISports, getSportsType>({
            query: ({ selectbox = "selectbox" }) => ({
                url: 'sports',
                params: {
                    selectbox,
                }
            })
        }),

    }),
})


export const { useGetSportsQuery } = sportsApi