export interface ISport {
    id: number;
    title: string;
    alias: string;
    eventsCount: number;
}

export interface ISports {
    data: ISport[];
}