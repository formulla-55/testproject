
interface Scout {
    id: number;
    lastName: string;
    firstName: string;
}

export interface IResultsData {
    id: number;
    champId: number;
    no: number;
    alias: string;
    date: number;
    results: string[];
    scout: Scout;
    status: number;
    state: number;
    isLive: number;
    isPayable: number;
    manualOddsTime?: any;
    host: string;
    champ: string;
    guest: string;
    sport: string;
    country?: any;
}

interface Pagination {
    first: number;
    before: number;
    previous: number;
    current: number;
    last: number;
    next: number;
    total_pages: number;
    total_items: number;
    limit: number;
}

export interface Results {
    data: IResultsData[];
    pagination: Pagination;
}



export interface IResultData {
    id: number;
    description: string;
    value: string;
    position: number;
}

export interface Result {
    data: IResultData[];
}
