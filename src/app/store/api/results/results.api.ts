import { createApi } from '@reduxjs/toolkit/query/react'
import { Dayjs } from 'dayjs'
import { baseQueryConfig } from '../../config/baseQuery'
import { IResultsData, Result, Results } from './results.type'


type getResultsType = {
    type: string,
    date?: Dayjs | string,
    page?: number,
    sport?: string,
    limit?: number,
    sort?: string
}
type getResultType = { id: number }

export const resultsApi = createApi({
    reducerPath: "results",
    tagTypes: ['Results', 'Result'],
    baseQuery: baseQueryConfig,
    endpoints: (build) => ({

        getResults: build.query<Results, getResultsType>({
            query: ({ type, date, page, limit, sport, sort = 'desc' }) => ({
                url: `results?${type}&${sort}`,
                params: {
                    sport,
                    date,
                    page,
                    limit,
                },
            }),
            providesTags: ['Results'],
            transformResponse: (response: Results) => {
                const groupByCategory = response.data.reduce((group: any, result: IResultsData) => {
                    const { champId } = result;
                    group[champId] = group[champId] ?? [];
                    group[champId].push(result);
                    return group;
                }, []);
                const results = { ...response, data: groupByCategory };
                return results
            }
        }),
        getResult: build.query<Result, getResultType>({
            query: ({ id }) => ({
                url: `results/${id}?extra`
            }),
            providesTags: ['Result']
        })

    }),
})


export const { useGetResultsQuery, useGetResultQuery } = resultsApi