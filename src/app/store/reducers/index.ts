import { combineReducers } from '@reduxjs/toolkit';
import { sportsApi } from './../api/sports/sports.api';
import { resultsApi } from './../api/results/results.api';

export default combineReducers({
    results: resultsApi.reducer,
    sports: sportsApi.reducer,
})