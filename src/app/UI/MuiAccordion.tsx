import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import Box from '@mui/material/Box';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { IResultsData } from '../store/api/results/results.type';
import { FC, useState } from 'react';
import moment from 'moment'
import { useGetResultQuery } from '../store/api/results/results.api';
import MuiAlert from '@mui/material/Alert';
import 'moment/locale/ru';

type MuiAccordionType = {
  results: IResultsData[]
}
const MuiAccordion: FC<MuiAccordionType> = ({ results }) => {
  const [expanded, setExpanded] = useState<number>(1);
  const { data: details, isFetching } = useGetResultQuery({ id: expanded })

  const handleChange = (panel: number) => (event: React.SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : 1);
  };


  return (
    <div>
      {results.map((result: IResultsData) => (
        <Accordion expanded={expanded === result.id} onChange={handleChange(result.id)} key={result.id}
          square
          sx={{ background: "#292c2d" }}
        >
          <AccordionSummary
            color='#292c2d'
            expandIcon={<ExpandMoreIcon />}
            aria-controls={`panel${result.id}f55-content`}
            id={`panel${result.id}f55-header`}
          >
            <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'} width={'100%'} gap={2}>
              <Box>
                <Typography fontSize={'0.7em'} color="GrayText">
                  {moment.unix(result.date).locale('ru').format('D MMM, h:mm')}
                </Typography>
                <Typography color="gainsboro" component='p' fontSize={'0.8em'} marginTop={'0.3em'} fontWeight={500}>{result.host} - {result.guest}</Typography>
              </Box>
              <Box display={'flex'} gap={0.8} flexWrap={'wrap'} justifyContent={'flex-end'}>
                {result.results.map((count, index) => <Typography fontSize={'0.8em'} color="GrayText" key={index}> {count}</Typography>)}
              </Box>
            </Box>
          </AccordionSummary>

          <AccordionDetails sx={{ backgroundColor: '#252525', paddingTop: 2 }}>
            {isFetching ? (
              <Typography component="p" padding={2} align="center">Пожалуйста подождите ...</Typography>
            ) : details ?
              details.data.length <= 0 ?
                (
                  <MuiAlert elevation={6} variant="filled" severity="error">Результаты не найдены !</MuiAlert>
                ) :
                details.data.map((detail) => (
                  <Typography color="gainsboro" component='p' fontSize={'0.8em'} marginTop={'0.3em'} fontWeight={500} key={detail.id} marginBottom={1}>{detail.description}</Typography>
                )) : null}
          </AccordionDetails>

        </Accordion>
      ))
      }
    </div >
  );
}
export default MuiAccordion;