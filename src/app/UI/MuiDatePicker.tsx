import { FC } from 'react'
import { Dayjs } from 'dayjs';
import 'dayjs/locale/ru';
import { TextField, Stack } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { MobileDatePicker } from '@mui/x-date-pickers';

interface IMuiDatePicker {
    date: Dayjs | null,
    handleChange: (newValue: Dayjs | null) => void
}

const MuiDatePicker: FC<IMuiDatePicker> = ({ date, handleChange }) => {

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={'ru'}>
            <Stack>
                <MobileDatePicker
                    label="Выберите дату"
                    inputFormat="MM/DD/YYYY"
                    value={date}
                    onChange={handleChange}
                    renderInput={(params) => <TextField
                        size="small"
                        {...params} />}
                />
            </Stack>
        </LocalizationProvider>
    );
}
export default MuiDatePicker;