import { Container, Box } from '@mui/material'
import { FC } from 'react'
import Footer from './footer'
import Header from './header'

const Layout: FC<any> = ({ children }) => {
    return (
        <Container disableGutters maxWidth={false}>
            <Header />
            <Box marginBottom={10}>
                {children}
            </Box>
            <Footer />
        </Container>
    )
}

export default Layout