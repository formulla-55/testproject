import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';


const Header = () => {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" sx={{ background: '#2c3031', paddingY: '0.5em' }}>
                <Toolbar>
                    <IconButton
                        size="small"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Button variant='outlined'>
                        <img
                            src={`/logo.png`}
                            alt={'logo'}
                            width={60}
                            loading="lazy"
                        />
                    </Button>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>

                    </Typography>

                    <Button variant='contained' sx={{ fontWeight: 600, textTransform: 'capitalize' }}>Вход</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export default Header