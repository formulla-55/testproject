import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import OutboxIcon from '@mui/icons-material/Outbox';
import StreamIcon from '@mui/icons-material/Stream';
import MenuOpenIcon from '@mui/icons-material/MenuOpen';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import { useState } from 'react';

const Footer = () => {
    const [value, setValue] = useState('recents');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    return (
        <Box sx={{ bottom: 0, width: "100%"}} position="fixed">
            <BottomNavigation sx={{ background: '#2c3031' }} value={value} onChange={handleChange}>
                <BottomNavigationAction
                    label="Лайв"
                    value="Лайв"
                    icon={<StreamIcon />}
                />
                <BottomNavigationAction
                    label="Линия"
                    value="Линия"
                    icon={<MenuOpenIcon />}
                />
                <BottomNavigationAction
                    label="Киберспорт"
                    value="Киберспорт"
                    icon={<SportsEsportsIcon />}
                />
                <BottomNavigationAction label="Кеио" value="Кеио" icon={<OutboxIcon />} />
            </BottomNavigation>
        </Box>

    );
}
export default Footer