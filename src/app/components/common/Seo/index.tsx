import { FC } from 'react'
import { Helmet } from "react-helmet";

interface ISeo {
    title?: string,
    description?: string
}
const Seo: FC<ISeo> = ({ title, description }) => {
    return (
        <Helmet>
            <meta charSet="utf-8" />
            <title>{title} | Formula 55</title>
            <meta name="description" content={description} />
        </Helmet>
    )
}

export default Seo