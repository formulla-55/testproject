import { SelectChangeEvent } from "@mui/material";
import { Dayjs } from "dayjs";

export interface IFilters {
    date: Dayjs | null,
    handleDateChange: (newValue: Dayjs | null) => void,
    sport: string,
    handleSportChange: (event: SelectChangeEvent<string>) => void,
}