import Box from '@mui/material/Box';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Unstable_Grid2';
import MuiDatePicker from '../../../../UI/MuiDatePicker';
import { FC } from 'react';
import { useGetSportsQuery } from '../../../../store/api/sports/sports.api';
import { IFilters } from './index.type';


const Filters: FC<IFilters> = ({ date, handleDateChange, sport, handleSportChange }) => {
    const { data: sports, isLoading } = useGetSportsQuery({ selectbox: 'selectbox' })

    return (
        <Box bgcolor={'#343839'} padding="20px">
            <Grid container spacing={2}>
                <Grid xs={6}>
                    <MuiDatePicker date={date} handleChange={handleDateChange} />
                </Grid>
                <Grid xs={6}>
                    <FormControl size="small" fullWidth>
                        <InputLabel id="demo-simple-select-label" sx={{ fontSize: "0.9em" }}>Все виды спорта</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={sport}
                            label="Все виды спорта"
                            onChange={handleSportChange}>
                            <MenuItem value={''}>Все виды спорта ...</MenuItem>
                            {isLoading ? (
                                <MenuItem>Пожалуйста подождите ... ...</MenuItem>
                            ) : sports &&
                            sports.data.map(sport => (<MenuItem value={sport.id} key={sport.id}>{sport.title}</MenuItem>))
                            }
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
        </Box>
    )
}

export default Filters