import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import { TabContext, TabPanel, TabList } from '@mui/lab';
import Filters from './Filters';
import { FC, useState } from 'react';
import { Divider, Pagination, SelectChangeEvent } from '@mui/material';
import MuiAccordion from '../../../../UI/MuiAccordion';
import dayjs, { Dayjs } from 'dayjs';
import { useGetResultsQuery } from '../../../../store/api/results/results.api';
import IsRenderResult from './IsRenderResult';


const Tabs: FC = () => {

    const [type, setType] = useState<string>('prematch');
    const [date, setDate] = useState<Dayjs | null>(null);
    const [page, setPage] = useState<number>(1);
    const [sport, setSport] = useState<string>('');

    const dateForm = date ? dayjs(date).format('DD-MM-YYYY') : dayjs().format('DD-MM-YYYY');

    const { data: results, isFetching, isError } = useGetResultsQuery({ type, date: dateForm, page, limit: 20, sort: 'desc', sport })


    const handleResultsTypeChange = (event: React.SyntheticEvent, newValue: string) => setType(newValue);
    const handleSportChange = (event: SelectChangeEvent<string>) => setSport(event.target.value);
    const handleDateChange = (newValue: Dayjs | null) => setDate(newValue);;
    const handlePagination = (event: React.ChangeEvent<unknown>, page: number) => setPage(page);

    return (
        <Box width="100%">
            <TabContext value={type}>

                <TabList
                    variant="fullWidth"
                    sx={{ background: '#222627' }}
                    onChange={handleResultsTypeChange}
                    textColor="primary"
                    indicatorColor="primary"
                    aria-label="change results type"
                >
                    <Tab value="prematch" label="Результаты" className='capitalize' sx={{ textTransform: 'capitalize', fontWeight: '700' }} key="prematch" />
                    <Tab value="live" label="Результаты лайва" sx={{ textTransform: 'capitalize', fontWeight: '700' }} key="live" />

                </TabList>

                <Filters date={date} handleDateChange={handleDateChange} sport={sport} handleSportChange={handleSportChange} />

                <Box bgcolor={'#343839'} border={"1px solid rgb(255 255 255 / 10%)"}>
                    <TabPanel value={type} sx={{ padding: 0 }}>

                        <IsRenderResult isFetching={isFetching} isError={isError} response={results}>
                            {results && results.data.map((result: any, index: number) => (
                                <Stack key={index} bgcolor={'#272723'}
                                    divider={<Divider orientation="horizontal" />}>
                                    <Typography component="h5" color={'black'} fontWeight={600} paddingX={"1.2em"} paddingY={'0.4em'} fontSize={'0.8em'} bgcolor="#fee900">{result[0]?.champ}</Typography>
                                    <MuiAccordion results={result} />
                                </Stack>
                            ))
                            }
                        </IsRenderResult>
                    </TabPanel>
                </Box>
            </TabContext>

            {!isFetching && results && (
                <Box marginTop={5} marginBottom={12} display='flex' justifyContent={'center'}>
                    <Pagination count={results?.pagination.total_pages} color="primary" size="large" onChange={handlePagination} page={page} />
                </Box>
            )}
        </Box>
    )
}

export default Tabs


