import Typography from '@mui/material/Typography';

interface IRenderResult {
    isFetching?: boolean,
    isError?: boolean,
    response?: any,
    children: any
}

const IsRenderResult = ({ isFetching, isError, response, children }: IRenderResult) => {
    if (isFetching) {
        return <Typography component="p" padding={2} align="center"> Пожалуйста подождите ...</Typography>
    }
    if (isError) {
        return <Typography component="p" padding={2} align="center"> Упс, что - то пошло ни так </Typography>
    }

    if (response && response.data.length <= 0) {
        return <Typography component="p" padding={2} align="center"> По вашему запросу ничего не найдено ...</Typography>
    }
    return children

}

export default IsRenderResult