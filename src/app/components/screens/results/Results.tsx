import Layout from "../../common/layout/layout"
import Seo from "../../common/Seo"
import Tabs from "./components/Tabs";


const ResultsScreen = () => {

    return (
        <Layout>
            <Seo title="Результаты"/>

            <Tabs />
        </Layout>
    )
}

export default ResultsScreen