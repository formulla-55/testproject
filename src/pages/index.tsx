import ResultsScreen from '../app/components/screens/results/Results';

function HomePage() {

    return (
        <ResultsScreen />
    );
}

export default HomePage;
